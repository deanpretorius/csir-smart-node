  #include <WiFi.h>
  #include <PubSubClient.h>

  #define BOARD_NUM 9
  //const char* ssid = "smart_campus";
  //const char* password =  "";
//  const char* mqttServer = "m21.cloudmqtt.com";
//  const int mqttPort = 10029;
//  const char* mqttUser = "mlokmjzc";
//  const char* mqttPassword = "P8634j3z6ofn";
  
  const char* mqttServer = "io.adafruit.com";
  const int mqttPort = 1883;
  const char* mqttUser = "deanpretorius";
  const char* mqttPassword = "d1f0a927e69d4cfa9044a3cd79c12328";
  
  char temp[8];
  String message;
  char mqtt_message[50];
  char* test_message;
  int len=0;
  WiFiClient espClient;
  PubSubClient client(espClient);

  void mqtt_setup(){
      client.setServer(mqttServer, mqttPort);
  
  while (!client.connected()) {
  Serial.println();
  Serial.println    ("+======+");
  Serial.println    ("| MQTT |");
  Serial.println    ("+======+ ");
  Serial.println();
  Serial.println("Connecting to MQTT...");
  
    if (client.connect("ESP32Client", mqttUser, mqttPassword )) {
  
      Serial.println("connected");
      delay(2000);
  
    } else {
  
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
  
    }
  }
}
  
 void mqtt() { 
  if (client.connect("ESP32Client", mqttUser, mqttPassword )) {
//    message = "Board " + ((String)BOARD_NUM) + " Temperature = ";       //Default start of string
    dtostrf(bme280.readTemperature(), 6, 2, temp);                      //floating point of temperature to string
//    message = message + temp + " [degrees C]   ";                       //CONTATINATING message before sending to MQTT broker
//    len = message.length();
//    message.toCharArray(mqtt_message, len + 1);                         //converting string to char array 
//    client.publish("deanpretorius/feeds/esp", mqtt_message);                           //Pushing message to MQTT broker

    message = temp;                       //CONTATINATING message before sending to MQTT broker
    len = message.length();
    message.toCharArray(mqtt_message, len + 1);                         //converting string to char array 
    client.publish("deanpretorius/feeds/esp", mqtt_message); 

    Serial.println("MQTT Message sent");       
    } else {
      Serial.println("Disconnected from MQTT broker");
    }
 }
  
  

