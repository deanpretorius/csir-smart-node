
//
//
// Send IR sequence
//
//
//
void sendIR(uint16_t buf[], uint16_t len) {
  for (uint16_t i = 0; i < len; i++) {
    if (i & 1) {                                    // second, third ... bits => buf[1], buf[3] etc. (the first bit is buf[0]) 
      space(buf[i]);
    } else {                                        // buf[0], buf[2] etc.
      mark(buf[i]);
    }
  }
  TOP_AND_FRONT_ledOff();                           // We potentially have ended with a mark(), so turn off the TOP and FRONT IR LEDs.
}


//
//
// Modulate the TOP and FRONT IR LEDs for the given period (usec).
//
// Args:
//   usec: The period of time to modulate the TOP and FRONT IR LEDs, in microseconds.
//
//
void mark(uint16_t usec) {
  ledcWrite(TOP_LEDC_CHANNEL, 5);                   //turn ON the pwm output on the TOP IR LED
  ledcWrite(FRONT_LEDC_CHANNEL, 5);                 //turn ON the pwm output on the FRONT IR LED
  if (usec == 0) return;
  delayMicroseconds(usec);
}



//
// Turn OFF the TOP and FRONT IR LEDs for the given period (usec).
//
// Args:
//   usec: The period of time to turn off the TOP and FRONT IR LEDs, in microseconds.
//
//
void space(uint32_t usec) {
    ledcWrite(TOP_LEDC_CHANNEL, 0);                 //turn OFF the pwm output on the TOP IR LED
    ledcWrite(FRONT_LEDC_CHANNEL, 0);               //turn OFF the pwm output on the FRONT IR LED
  if (usec == 0) return;
  delayMicroseconds(usec);
}


//
//
//
// Turn off the TOP and FRONT IR LEDs.
//
//
//
void TOP_AND_FRONT_ledOff() {
    ledcWrite(FRONT_LEDC_CHANNEL, 0);             //turn OFF the pwm output on the FRONT IR LED
    ledcWrite(TOP_LEDC_CHANNEL, 0);               //turn OFF the pwm output on the TOP IR LED
}


