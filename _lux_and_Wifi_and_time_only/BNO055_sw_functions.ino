//===================================================================================================================
//====== Set of useful BNO055 functions to access acceleration. gyroscope, magnetometer, and temperature data
//===================================================================================================================




/*
==================================================================
                    _                      _ _____        _        
                   | |   /\               | |  __ \      | |       
 _ __ ___  __ _  __| |  /  \   ___ ___ ___| | |  | | __ _| |_ __ _ 
| '__/ _ \/ _` |/ _` | / /\ \ / __/ __/ _ \ | |  | |/ _` | __/ _` |
| | |  __/ (_| | (_| |/ ____ \ (_| (_|  __/ | |__| | (_| | || (_| |
|_|  \___|\__,_|\__,_/_/    \_\___\___\___|_|_____/ \__,_|\__\__,_|
                                                                   
                   
==================================================================
*/
void readAccelData(int16_t * destination)
{
  uint8_t rawData[6];  // x/y/z accel register data stored here
  readBytes(BNO055_ADDRESS, BNO055_ACC_DATA_X_LSB, 6, &rawData[0]);                             // Read the six raw data registers into data array
  destination[0] = ((int16_t)rawData[1] << 8) | rawData[0] ;                                    // Turn the MSB and LSB into a signed 16-bit value
  destination[1] = ((int16_t)rawData[3] << 8) | rawData[2] ;  
  destination[2] = ((int16_t)rawData[5] << 8) | rawData[4] ; 
}



/*
==================================================================
                    _  _____                 _____        _        
                   | |/ ____|               |  __ \      | |       
 _ __ ___  __ _  __| | |  __ _   _ _ __ ___ | |  | | __ _| |_ __ _ 
| '__/ _ \/ _` |/ _` | | |_ | | | | '__/ _ \| |  | |/ _` | __/ _` |
| | |  __/ (_| | (_| | |__| | |_| | | | (_) | |__| | (_| | || (_| |
|_|  \___|\__,_|\__,_|\_____|\__, |_|  \___/|_____/ \__,_|\__\__,_|
                              __/ |                                
                             |___/      
                   
==================================================================
*/
void readGyroData(int16_t * destination)
{
  uint8_t rawData[6];  // x/y/z gyro register data stored here
  readBytes(BNO055_ADDRESS, BNO055_GYR_DATA_X_LSB, 6, &rawData[0]);                         // Read the six raw data registers sequentially into data array
  destination[0] = ((int16_t)rawData[1] << 8) | rawData[0] ;                                // Turn the MSB and LSB into a signed 16-bit value
  destination[1] = ((int16_t)rawData[3] << 8) | rawData[2] ;  
  destination[2] = ((int16_t)rawData[5] << 8) | rawData[4] ; 
}



/*
==================================================================

                    _ __  __             _____        _        
                   | |  \/  |           |  __ \      | |       
 _ __ ___  __ _  __| | \  / | __ _  __ _| |  | | __ _| |_ __ _ 
| '__/ _ \/ _` |/ _` | |\/| |/ _` |/ _` | |  | |/ _` | __/ _` |
| | |  __/ (_| | (_| | |  | | (_| | (_| | |__| | (_| | || (_| |
|_|  \___|\__,_|\__,_|_|  |_|\__,_|\__, |_____/ \__,_|\__\__,_|
                                    __/ |                      
                                   |___/                          
==================================================================
*/
int8_t readGyroTempData()
{
  return readByte(BNO055_ADDRESS, BNO055_TEMP);                                               // Read the two raw data registers sequentially into data array 
}



/*
==================================================================

                    _ __  __             _____        _        
                   | |  \/  |           |  __ \      | |       
 _ __ ___  __ _  __| | \  / | __ _  __ _| |  | | __ _| |_ __ _ 
| '__/ _ \/ _` |/ _` | |\/| |/ _` |/ _` | |  | |/ _` | __/ _` |
| | |  __/ (_| | (_| | |  | | (_| | (_| | |__| | (_| | || (_| |
|_|  \___|\__,_|\__,_|_|  |_|\__,_|\__, |_____/ \__,_|\__\__,_|
                                    __/ |                      
                                   |___/                       
                   
==================================================================
*/
void readMagData(int16_t * destination)
{
  uint8_t rawData[6];  // x/y/z gyro register data stored here
  readBytes(BNO055_ADDRESS, BNO055_MAG_DATA_X_LSB, 6, &rawData[0]);                           // Read the six raw data registers sequentially into data array
  destination[0] = ((int16_t)rawData[1] << 8) | rawData[0] ;                                  // Turn the MSB and LSB into a signed 16-bit value
  destination[1] = ((int16_t)rawData[3] << 8) | rawData[2] ;  
  destination[2] = ((int16_t)rawData[5] << 8) | rawData[4] ;
}



/*
==================================================================
                    _  ____              _   _____        _        
                   | |/ __ \            | | |  __ \      | |       
 _ __ ___  __ _  __| | |  | |_   _  __ _| |_| |  | | __ _| |_ __ _ 
| '__/ _ \/ _` |/ _` | |  | | | | |/ _` | __| |  | |/ _` | __/ _` |
| | |  __/ (_| | (_| | |__| | |_| | (_| | |_| |__| | (_| | || (_| |
|_|  \___|\__,_|\__,_|\___\_\\__,_|\__,_|\__|_____/ \__,_|\__\__,_|
                   
==================================================================
*/
void readQuatData(int16_t * destination)
{
  uint8_t rawData[8];  // x/y/z gyro register data stored here
  readBytes(BNO055_ADDRESS, BNO055_QUA_DATA_W_LSB, 8, &rawData[0]);                             // Read the six raw data registers sequentially into data array
  destination[0] = ((int16_t)rawData[1] << 8) | rawData[0] ;                                    // Turn the MSB and LSB into a signed 16-bit value
  destination[1] = ((int16_t)rawData[3] << 8) | rawData[2] ;  
  destination[2] = ((int16_t)rawData[5] << 8) | rawData[4] ;
  destination[3] = ((int16_t)rawData[7] << 8) | rawData[6] ;
}



/*
==================================================================

                    _ ______      _ _____        _        
                   | |  ____|    | |  __ \      | |       
 _ __ ___  __ _  __| | |__  _   _| | |  | | __ _| |_ __ _ 
| '__/ _ \/ _` |/ _` |  __|| | | | | |  | |/ _` | __/ _` |
| | |  __/ (_| | (_| | |___| |_| | | |__| | (_| | || (_| |
|_|  \___|\__,_|\__,_|______\__,_|_|_____/ \__,_|\__\__,_|
                                                                             
==================================================================
*/
void readEulData(int16_t * destination)
{
  uint8_t rawData[6];  // x/y/z gyro register data stored here
  readBytes(BNO055_ADDRESS, BNO055_EUL_HEADING_LSB, 6, &rawData[0]);                      // Read the six raw data registers sequentially into data array
  destination[0] = ((int16_t)rawData[1] << 8) | rawData[0] ;                              // Turn the MSB and LSB into a signed 16-bit value
  destination[1] = ((int16_t)rawData[3] << 8) | rawData[2] ;  
  destination[2] = ((int16_t)rawData[5] << 8) | rawData[4] ;
}



/*
==================================================================
                    _ _      _____          _____        _        
                   | | |    |_   _|   /\   |  __ \      | |       
 _ __ ___  __ _  __| | |      | |    /  \  | |  | | __ _| |_ __ _ 
| '__/ _ \/ _` |/ _` | |      | |   / /\ \ | |  | |/ _` | __/ _` |
| | |  __/ (_| | (_| | |____ _| |_ / ____ \| |__| | (_| | || (_| |
|_|  \___|\__,_|\__,_|______|_____/_/    \_\_____/ \__,_|\__\__,_|
                                                                  
                   
==================================================================
*/
void readLIAData(int16_t * destination)
{
  uint8_t rawData[6];                                                                     // x/y/z gyro register data stored here
  readBytes(BNO055_ADDRESS, BNO055_LIA_DATA_X_LSB, 6, &rawData[0]);                       // Read the six raw data registers sequentially into data array
  destination[0] = ((int16_t)rawData[1] << 8) | rawData[0] ;                              // Turn the MSB and LSB into a signed 16-bit value
  destination[1] = ((int16_t)rawData[3] << 8) | rawData[2] ;  
  destination[2] = ((int16_t)rawData[5] << 8) | rawData[4] ;
}



/*
==================================================================
                    _  _____ _______      _______        _        
                   | |/ ____|  __ \ \    / /  __ \      | |       
 _ __ ___  __ _  __| | |  __| |__) \ \  / /| |  | | __ _| |_ __ _ 
| '__/ _ \/ _` |/ _` | | |_ |  _  / \ \/ / | |  | |/ _` | __/ _` |
| | |  __/ (_| | (_| | |__| | | \ \  \  /  | |__| | (_| | || (_| |
|_|  \___|\__,_|\__,_|\_____|_|  \_\  \/   |_____/ \__,_|\__\__,_|
                                                                
==================================================================
*/
void readGRVData(int16_t * destination)
{
  uint8_t rawData[6];                                                                 // x/y/z gravity register data stored here
  readBytes(BNO055_ADDRESS, BNO055_GRV_DATA_X_LSB, 6, &rawData[0]);                   // Read the six raw data registers sequentially into data array
  destination[0] = ((int16_t)rawData[1] << 8) | rawData[0] ;                          // Turn the MSB and LSB into a signed 16-bit value
  destination[1] = ((int16_t)rawData[3] << 8) | rawData[2] ;  
  destination[2] = ((int16_t)rawData[5] << 8) | rawData[4] ;
}



/*
==================================================================

 _       _ _   ____  _   _  ____   ___  _____ _____ 
(_)     (_) | |  _ \| \ | |/ __ \ / _ \| ____| ____|
 _ _ __  _| |_| |_) |  \| | |  | | | | | |__ | |__  
| | '_ \| | __|  _ <| . ` | |  | | | | |___ \|___ \ 
| | | | | | |_| |_) | |\  | |__| | |_| |___) |___) |
|_|_| |_|_|\__|____/|_| \_|\____/ \___/|____/|____/ 
                   
                   
==================================================================
*/
void initBNO055() {
   
   writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, CONFIGMODE );                                     // Select BNO055 config mode
   delay(25);
   
   writeByte(BNO055_ADDRESS, BNO055_PAGE_ID, 0x01);                                             // Select page 1 to configure sensors
   
   writeByte(BNO055_ADDRESS, BNO055_ACC_CONFIG, APwrMode << 5 | Abw << 2 | Ascale );            // Configure ACC
   
   writeByte(BNO055_ADDRESS, BNO055_GYRO_CONFIG_0, Gbw << 3 | Gscale );                         // Configure GYR
   writeByte(BNO055_ADDRESS, BNO055_GYRO_CONFIG_1, GPwrMode);
   
   writeByte(BNO055_ADDRESS, BNO055_MAG_CONFIG, MPwrMode << 5 | MOpMode << 3 | Modr );          // Configure MAG
   
   writeByte(BNO055_ADDRESS, BNO055_PAGE_ID, 0x00);                                             // Select page 0 to read sensors
    
   writeByte(BNO055_ADDRESS, BNO055_TEMP_SOURCE, 0x01 );                                        // Select BNO055 gyro temperature source
 
   writeByte(BNO055_ADDRESS, BNO055_UNIT_SEL, 0x01 );                                           // Select BNO055 sensor units (temperature in degrees C, rate in dps, accel in mg)
  
   writeByte(BNO055_ADDRESS, BNO055_PWR_MODE, PWRMode );                                        // Select BNO055 system power mode
    
   writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, OPRMode );                                        // Select BNO055 system operation mode
   delay(25);
   
}



/*
==============================================================================================

                    _                        _____      _ ____  _   _  ____   ___  _____ _____ 
                   | |                      / ____|    | |  _ \| \ | |/ __ \ / _ \| ____| ____|
  __ _  ___ ___ ___| | __ _ _   _ _ __ ___ | |     __ _| | |_) |  \| | |  | | | | | |__ | |__  
 / _` |/ __/ __/ _ \ |/ _` | | | | '__/ _ \| |    / _` | |  _ <| . ` | |  | | | | |___ \|___ \ 
| (_| | (_| (_|  __/ | (_| | |_| | | | (_) | |___| (_| | | |_) | |\  | |__| | |_| |___) |___) |
 \__,_|\___\___\___|_|\__, |\__, |_|  \___/ \_____\__,_|_|____/|_| \_|\____/ \___/|____/|____/ 
                       __/ | __/ |                                                             
                      |___/ |___/                                                           
===============================================================================================
*/
void accelgyroCalBNO055(float * dest1, float * dest2) 
{
  uint8_t data[6];                                                                                  // data array to hold accelerometer and gyro x, y, z, data
  uint16_t ii = 0, sample_count = 0;
  int32_t gyro_bias[3]  = {0, 0, 0}, accel_bias[3] = {0, 0, 0};

  Serial.println();
  Serial.println("Accel/Gyro Calibration");
  Serial.println("----------------------");
 
  Serial.print("Put device on a level surface and keep motionless! Wait...");
  digitalWrite(LED_PIN_ORANGE,  HIGH); 
  delay(4000);                                                                                      // This 4000 ms delay must be maintained
  digitalWrite(LED_PIN_ORANGE,  LOW); 
  
   writeByte(BNO055_ADDRESS, BNO055_PAGE_ID, 0x00);                                                 // Select page 0 to read sensors
   
   writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, CONFIGMODE );                                         // Select BNO055 system operation mode as AMG for calibration
   delay(25);
   writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, AMG);
   
                                                                                                    // In NDF fusion mode, accel full scale is at +/- 4g, ODR is 62.5 Hz, set it the same here
   writeByte(BNO055_ADDRESS, BNO055_ACC_CONFIG, APwrMode << 5 | Abw << 2 | AFS_4G );

   sample_count = 256;
   for(ii = 0; ii < sample_count; ii++) {


    int16_t accel_temp[3] = {0, 0, 0};
    readBytes(BNO055_ADDRESS, BNO055_ACC_DATA_X_LSB, 6, &data[0]);                                  // Read the six raw data registers into data array
    accel_temp[0] = (int16_t) (((int16_t)data[1] << 8) | data[0]) ;                                 // Form signed 16-bit integer for each sample in FIFO
    accel_temp[1] = (int16_t) (((int16_t)data[3] << 8) | data[2]) ;
    accel_temp[2] = (int16_t) (((int16_t)data[5] << 8) | data[4]) ;
    accel_bias[0]  += (int32_t) accel_temp[0];
    accel_bias[1]  += (int32_t) accel_temp[1];
    accel_bias[2]  += (int32_t) accel_temp[2];
    
    digitalWrite(LED_PIN_ORANGE,  HIGH);   Serial.print(".");                             // the 20ms (10 +10) delay below must be maintained
    delay(5);                                                                            // at 62.5 Hz ODR, new accel data is available every 16 ms => delay 20ms (10+10)in total
    digitalWrite(LED_PIN_ORANGE,  LOW);
    delay(15);                                                                            // this delay plus one ubove must add up to 20ms
 
   }  //for(ii
   
    accel_bias[0]  /= (int32_t) sample_count;                                             // get average accel bias in mg
    accel_bias[1]  /= (int32_t) sample_count;
    accel_bias[2]  /= (int32_t) sample_count;
    
  if(accel_bias[2] > 0L) {accel_bias[2] -= (int32_t) 1000;}                               // Remove gravity from the z-axis accelerometer bias calculation
  else {accel_bias[2] += (int32_t) 1000;}

    dest1[0] = (float) accel_bias[0];                                                     // save accel biases in mg for use in main program
    dest1[1] = (float) accel_bias[1];                                                     // accel data is 1 LSB/mg
    dest1[2] = (float) accel_bias[2];          

                                                                                          // In NDF fusion mode, gyro full scale is at +/- 2000 dps, ODR is 32 Hz
   writeByte(BNO055_ADDRESS, BNO055_GYRO_CONFIG_0, Gbw << 3 | GFS_2000DPS );
   writeByte(BNO055_ADDRESS, BNO055_GYRO_CONFIG_1, GPwrMode);

   for(ii = 0; ii < sample_count; ii++) {
 
    int16_t gyro_temp[3] = {0, 0, 0};
    
    readBytes(BNO055_ADDRESS, BNO055_GYR_DATA_X_LSB, 6, &data[0]);                        // Read the six raw data registers into data array
    gyro_temp[0] = (int16_t) (((int16_t)data[1] << 8) | data[0]) ;                        // Form signed 16-bit integer for each sample in FIFO
    gyro_temp[1] = (int16_t) (((int16_t)data[3] << 8) | data[2]) ;
    gyro_temp[2] = (int16_t) (((int16_t)data[5] << 8) | data[4]) ;
    gyro_bias[0]  += (int32_t) gyro_temp[0];
    gyro_bias[1]  += (int32_t) gyro_temp[1];
    gyro_bias[2]  += (int32_t) gyro_temp[2];

    Serial.print("|");                                                                    // The delays here must add up to 35ms
    digitalWrite(LED_PIN_ORANGE,  HIGH);
    delay(5);                                                                            // at 32 Hz ODR, new gyro data available every 31 ms => delay 35ms
    digitalWrite(LED_PIN_ORANGE,  LOW);
    delay(30); 
    
   }

    gyro_bias[0]  /= (int32_t) sample_count;                                              // get average gyro bias in counts
    gyro_bias[1]  /= (int32_t) sample_count;
    gyro_bias[2]  /= (int32_t) sample_count;
 
    dest2[0] = (float) gyro_bias[0]/16.;                                                  // save gyro biases in dps for use in main program
    dest2[1] = (float) gyro_bias[1]/16.;                                                  // gyro data is 16 LSB/dps
    dest2[2] = (float) gyro_bias[2]/16.;          

                                                                                          // Return to config mode to write accelerometer biases in offset register
                                                                                          // This offset register is only used while in fusion mode when accelerometer full-scale is +/- 4g
  writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, CONFIGMODE );
  delay(25);
  
                                                                                          //write biases to accelerometer offset registers ad 16 LSB/dps
  writeByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_X_LSB, (int16_t)accel_bias[0] & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_X_MSB, ((int16_t)accel_bias[0] >> 8) & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_Y_LSB, (int16_t)accel_bias[1] & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_Y_MSB, ((int16_t)accel_bias[1] >> 8) & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_Z_LSB, (int16_t)accel_bias[2] & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_Z_MSB, ((int16_t)accel_bias[2] >> 8) & 0xFF);
  
  // Check that offsets were properly written to offset registers
//  Serial.println("Average accelerometer bias = "); 
//  Serial.println((int16_t)((int16_t)readByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_X_MSB) << 8 | readByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_X_LSB))); 
//  Serial.println((int16_t)((int16_t)readByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_Y_MSB) << 8 | readByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_Y_LSB))); 
//  Serial.println((int16_t)((int16_t)readByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_Z_MSB) << 8 | readByte(BNO055_ADDRESS, BNO055_ACC_OFFSET_Z_LSB)));

   //write biases to gyro offset registers
  writeByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_X_LSB, (int16_t)gyro_bias[0] & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_X_MSB, ((int16_t)gyro_bias[0] >> 8) & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_Y_LSB, (int16_t)gyro_bias[1] & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_Y_MSB, ((int16_t)gyro_bias[1] >> 8) & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_Z_LSB, (int16_t)gyro_bias[2] & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_Z_MSB, ((int16_t)gyro_bias[2] >> 8) & 0xFF);
  
  // Select BNO055 system operation mode
  writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, OPRMode );

 // Check that offsets were properly written to offset registers
//  Serial.println("Average gyro bias = "); 
//  Serial.println((int16_t)((int16_t)readByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_X_MSB) << 8 | readByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_X_LSB))); 
//  Serial.println((int16_t)((int16_t)readByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_Y_MSB) << 8 | readByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_Y_LSB))); 
//  Serial.println((int16_t)((int16_t)readByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_Z_MSB) << 8 | readByte(BNO055_ADDRESS, BNO055_GYR_OFFSET_Z_LSB)));

  Serial.println();
  Serial.println("Accel/Gyro Calibration done!");
  Serial.println();
}



/*
===========================================================================

                        _____      _ ____  _   _  ____   ___  _____ _____ 
                       / ____|    | |  _ \| \ | |/ __ \ / _ \| ____| ____|
 _ __ ___   __ _  __ _| |     __ _| | |_) |  \| | |  | | | | | |__ | |__  
| '_ ` _ \ / _` |/ _` | |    / _` | |  _ <| . ` | |  | | | | |___ \|___ \ 
| | | | | | (_| | (_| | |___| (_| | | |_) | |\  | |__| | |_| |___) |___) |
|_| |_| |_|\__,_|\__, |\_____\__,_|_|____/|_| \_|\____/ \___/|____/|____/ 
                  __/ |                                                   
                 |___/                                                    
============================================================================
*/
void magCalBNO055(float * dest1) 
{
  uint8_t data[6];                                                                  // data array to hold mag x, y, z, data
  uint16_t ii = 0, sample_count = 0;
  int32_t mag_bias[3] = {0, 0, 0};
  //int16_t mag_max[3] = {0x8000, 0x8000, 0x8000}, mag_min[3] = {0x7FFF, 0x7FFF, 0x7FFF};

  Serial.println();
  Serial.println("Mag Calibration");
  Serial.println("---------------");
  Serial.print("Wave device in a figure eight ");                                   // this procedure is demonstrated at https://www.youtube.com/watch?v=sP3d00Hr14o
  
  digitalWrite(LED_PIN_ORANGE,  HIGH);
  delay(4000);                                                                      // This delay must be maintained at 4000ms
  digitalWrite(LED_PIN_ORANGE,  LOW);
      
  // Select page 0 to read sensors
   writeByte(BNO055_ADDRESS, BNO055_PAGE_ID, 0x00);
   
   // Select BNO055 system operation mode as AMG for calibration
   writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, CONFIGMODE );
   delay(25);
   writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, AMG );

                                                                                    // In NDF fusion mode, mag data is in 16 LSB/microTesla, ODR is 20 Hz in forced mode
   sample_count = 256;
   
  for(ii = 0; ii < sample_count; ii++) {

    int16_t mag_temp[3] = {0, 0, 0};
    
    readBytes(BNO055_ADDRESS, BNO055_MAG_DATA_X_LSB, 6, &data[0]);            // Read the six raw data registers into data array
    
    mag_temp[0] = (int16_t) (((int16_t)data[1] << 8) | data[0]) ;             // Form signed 16-bit integer for each sample in FIFO
    mag_temp[1] = (int16_t) (((int16_t)data[3] << 8) | data[2]) ;
    mag_temp[2] = (int16_t) (((int16_t)data[5] << 8) | data[4]) ;
    for (int jj = 0; jj < 3; jj++) {
      if (ii == 0) {
        //mag_max[jj] = mag_temp[jj];                                         // Offsets may be large enough that mag_temp[i] may not be bipolar! 
        //mag_min[jj] = mag_temp[jj];                                         // This prevents max or min being pinned to 0 if the values are unipolar...
      } else {
        //if(mag_temp[jj] > mag_max[jj]) mag_max[jj] = mag_temp[jj];
        //if(mag_temp[jj] < mag_min[jj]) mag_min[jj] = mag_temp[jj];
      }
    }
                                                                               // the delays here must add up to 105ms 
    digitalWrite(LED_PIN_ORANGE,  HIGH);
    Serial.print(".");
    delay(5);                                                                 // at 10 Hz ODR, new mag data is available every 100 ms => delays must add up to 105 ms
    digitalWrite(LED_PIN_ORANGE,  LOW);
    delay(100);  
    
  }  //for(ii = 0



 //   Serial.println("mag x min/max:"); Serial.println(mag_max[0]); Serial.println(mag_min[0]);
 //   Serial.println("mag y min/max:"); Serial.println(mag_max[1]); Serial.println(mag_min[1]);
 //   Serial.println("mag z min/max:"); Serial.println(mag_max[2]); Serial.println(mag_min[2]);

   // mag_bias[0]  = (mag_max[0] + mag_min[0])/2;  // get average x mag bias in counts
    //mag_bias[1]  = (mag_max[1] + mag_min[1])/2;  // get average y mag bias in counts
    //mag_bias[2]  = (mag_max[2] + mag_min[2])/2;  // get average z mag bias in counts
    
    dest1[0] = (float) mag_bias[0] / 1.6;  // save mag biases in mG for use in main program
    dest1[1] = (float) mag_bias[1] / 1.6;  // mag data is 1.6 LSB/mg
    dest1[2] = (float) mag_bias[2] / 1.6;          

                                                                                                          // Return to config mode to write mag biases in offset register
                                                                                                          // This offset register is only used while in fusion mode when magnetometer sensitivity is 16 LSB/microTesla
  writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, CONFIGMODE );
  delay(25);
  
                                                                                                          //write biases to magnetometer offset registers as 16 LSB/microTesla
  writeByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_X_LSB, (int16_t)mag_bias[0] & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_X_MSB, ((int16_t)mag_bias[0] >> 8) & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_Y_LSB, (int16_t)mag_bias[1] & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_Y_MSB, ((int16_t)mag_bias[1] >> 8) & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_Z_LSB, (int16_t)mag_bias[2] & 0xFF);
  writeByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_Z_MSB, ((int16_t)mag_bias[2] >> 8) & 0xFF);
 
                                                                                                          // Check that offsets were properly written to offset registers
//  Serial.println("Average magnetometer bias = "); 
//  Serial.println((int16_t)((int16_t)readByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_X_MSB) << 8 | readByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_X_LSB))); 
//  Serial.println((int16_t)((int16_t)readByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_Y_MSB) << 8 | readByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_Y_LSB))); 
//  Serial.println((int16_t)((int16_t)readByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_Z_MSB) << 8 | readByte(BNO055_ADDRESS, BNO055_MAG_OFFSET_Z_LSB)));


  // Select BNO055 system operation mode
  writeByte(BNO055_ADDRESS, BNO055_OPR_MODE, OPRMode );

  Serial.println();    
   Serial.println("Mag Calibration done!");
   Serial.println();
}



/*
==================================================================
               _ _   ____  _   _  ____   ___  _____ _____ 
              (_) | |  _ \| \ | |/ __ \ / _ \| ____| ____|
__      ____ _ _| |_| |_) |  \| | |  | | | | | |__ | |__  
\ \ /\ / / _` | | __|  _ <| . ` | |  | | | | |___ \|___ \ 
 \ V  V / (_| | | |_| |_) | |\  | |__| | |_| |___) |___) |
  \_/\_/ \__,_|_|\__|____/|_| \_|\____/ \___/|____/|____/ 

Ref: App note, p4 - the BNO055 needs time to start up  

==================================================================
*/                                                         

void waitBNO055() {
                                                                                                          // The delay must be a minimum of 650ms
                                                                                                         
  delay(700);
}  

