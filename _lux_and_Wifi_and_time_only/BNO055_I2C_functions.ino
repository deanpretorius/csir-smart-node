
/*
==================================================================

I2C read/write functions for the BNO055 sensor           

==================================================================
*/




/*
==================================================================

               _ _       ____        _       
              (_) |     |  _ \      | |      
__      ___ __ _| |_ ___| |_) |_   _| |_ ___ 
\ \ /\ / / '__| | __/ _ \  _ <| | | | __/ _ \
 \ V  V /| |  | | ||  __/ |_) | |_| | ||  __/
  \_/\_/ |_|  |_|\__\___|____/ \__, |\__\___|
                                __/ |        
                               |___/       
                               
I2C read/write functions for the BNO055 sensor           

==================================================================
*/
void writeByte(uint8_t address, uint8_t subAddress, uint8_t data)
{
  Wire.beginTransmission(address);  // Initialize the Tx buffer
  Wire.write(subAddress);           // Put slave register address in Tx buffer
  Wire.write(data);                 // Put data in Tx buffer
  Wire.endTransmission();           // Send the Tx buffer
}




/*
==================================================================

                    _ ____        _       
                   | |  _ \      | |      
 _ __ ___  __ _  __| | |_) |_   _| |_ ___ 
| '__/ _ \/ _` |/ _` |  _ <| | | | __/ _ \
| | |  __/ (_| | (_| | |_) | |_| | ||  __/
|_|  \___|\__,_|\__,_|____/ \__, |\__\___|
                             __/ |        
                            |___/        

==================================================================
*/
uint8_t readByte(uint8_t address, uint8_t subAddress)
{
  uint8_t data;                            // `data` will store the register data   
  Wire.beginTransmission(address);         // Initialize the Tx buffer
  Wire.write(subAddress);                  // Put slave register address in Tx buffer, queue bytes for transmission
  Wire.endTransmission(false);             // Send the Tx buffer, but send a restart to keep connection alive
  Wire.requestFrom(address, 1);            // Read one byte from slave register address. Used by the master to request bytes from a slave device. The bytes may then be retrieved with the available() and read() functions.
  data = Wire.read();                      // Fill Rx buffer with result
  return data;                             // Return data read from slave register
}




/*
==================================================================


                    _ ____        _            
                   | |  _ \      | |           
 _ __ ___  __ _  __| | |_) |_   _| |_ ___  ___ 
| '__/ _ \/ _` |/ _` |  _ <| | | | __/ _ \/ __|
| | |  __/ (_| | (_| | |_) | |_| | ||  __/\__ \
|_|  \___|\__,_|\__,_|____/ \__, |\__\___||___/
                             __/ |             
                            |___/           

==================================================================
*/
void readBytes(uint8_t address, uint8_t subAddress, uint8_t count, uint8_t * dest)
{  
  Wire.beginTransmission(address);            // Initialize the Tx buffer
  Wire.write(subAddress);                     // Put slave register address in Tx buffer
  Wire.endTransmission(false);                // Send the Tx buffer, but send a restart to keep connection alive
  uint8_t i = 0;
  Wire.requestFrom(address, count);           // Read bytes from slave register address 
  while (Wire.available()) {
        dest[i++] = Wire.read(); }            // Put read results in the Rx buffer
}
