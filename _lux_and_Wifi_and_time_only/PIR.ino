

//
//
// Interrupt code based on https://techtutorialsx.com/2017/09/30/esp32-arduino-external-interrupts/ and https://github.com/espressif/arduino-esp32/issues/855
// modified by Andrew Smith 9 April 2018
//
//
//

void IRAM_ATTR EKMC1603112handleInterrupt() {    
//
// Perform the decrement of the variable inside a critical section, which we declare using a portENTER_CRITICAL and a portEXIT_CRITICAL macro. 
//  These calls both receive as input the address of the previously declared global portMUX_TYPE variable.  
// 
// The interrupt handling routine uses the IRAM_ATTR attribute in order for the compiler to place the code in IRAM. 
//    Also, interrupt handling routines should only call functions also placed in IRAM, as can be seen here in the IDF documentation. See https://github.com/espressif/arduino-esp32/issues/855
//    IRAM_ATTR tells the complier that this code Must always be in the ESP32's IRAM, the limited 128k IRAM.  use it sparingly.
//
// Enclose this operation in a critical section which we declare by calling the portENTER_CRITICAL_ISR and portExit_CRITICAL_ISR macros. 
//    They also both receive as input the address of the global portMUX_TYPE variable.  
//

  portENTER_CRITICAL_ISR(&mux);       //  Disable interrupts when writing on a variable that is shared with an interrupt to ensure that there is no concurrent access to it between the main code and the ISR.
                                      //
                                      //  In the Arduino environment, we usually have the NoInterrupts and Interrupts function to disable and re-enable interrupts. 
                                      //    However, at the time of writing, these functions were not yet implemented in the ESP32 Arduino core.
                                      //
                                      //  About portENTER_CRITICAL: [https://github.com/espressif/arduino-esp32/blob/master/tools/sdk/include/freertos/freertos/portmacro.h]
                                      //   For an introduction, see "Critical Sections & Disabling Interrupts" in docs/api-guides/freertos-smp.rst
                                      //   The original portENTER_CRITICAL only disabled the ISRs. This is enough for single-CPU operation: by
                                      //      disabling the interrupts, there is no task switch so no other tasks can meddle in the data, and because
                                      //      interrupts are disabled, ISRs can't corrupt data structures either.
                                      //      For multiprocessing, things get a bit more hairy. First of all, disabling the interrupts doesn't stop
                                      //      the tasks or ISRs on the other processors meddling with our CPU. For tasks, this is solved by adding
                                      //      a spinlock to the portENTER_CRITICAL macro. A task running on the other CPU accessing the same data will
                                      //      spinlock in the portENTER_CRITICAL code until the first CPU is done.
                                      //      For ISRs, we now also need muxes: while portENTER_CRITICAL disabling interrupts will stop ISRs on the same
                                      //      CPU from meddling with the data, it does not stop interrupts on the other cores from interfering with the
                                      //      data. For this, we also use a spinlock in the routines called by the ISR, but these spinlocks
                                      //      do not disable the interrupts (because they already are).
                                      //      This all assumes that interrupts are either entirely disabled or enabled. Interrupt priority levels
                                      //      will break this scheme. 
                                      //  portENTER_CRITICAL and portENTER_CRITICAL_ISR both alias vTaskEnterCritical, meaning that either function can be called both from ISR as well as task context. 
                                      //    This is not standard FreeRTOS behaviour; please keep this in mind if you need any compatibility with other FreeRTOS implementations. 
                                      //  The ESP32 has no hardware method for cores to disable each other’s interrupts. [http://esp-idf.readthedocs.io/en/latest/api-guides/freertos-smp.html?highlight=portenter_critical#critical-sections-disabling-interrupts]
                                      //    Calling portDISABLE_INTERRUPTS() will have no effect on the interrupts of the other core. 
                                      //    Therefore, disabling interrupts is NOT a valid protection method against simultaneous access to shared data as 
                                      //    it leaves the other core free to access the data even if the current core has disabled its own interrupts.  
                                      //    Therefore the ESP-IDF (Espressif IoT Development Framework)implements critical sections using mutexes, and calls to enter or exit a critical must 
                                      //    provide a mutex that is associated with a shared resource requiring access protection. 
                                      //    When entering a critical section in ESP-IDF FreeRTOS, the calling core will disable its scheduler and interrupts similar to the vanilla FreeRTOS implementation. 
                                      //    However, the calling core will also take the mutex whilst the other core is left unaffected during the critical section. If the other core attempts to take the same mutex, 
                                      //    it will spin until the mutex is released. Therefore, the ESP-IDF FreeRTOS implementation of critical sections allows a core to have protected access to a shared resource 
                                      //    without disabling the other core. The other core will only be affected if it tries to concurrently access the same resource.
                                      
  EKMC1603112interruptCounter++;                 //  This counter approach is better than using a flag since if multiple interrupts occur without the main code being able to handle them all, we will not loose any events. 
  portEXIT_CRITICAL_ISR(&mux);
}

