/* 
 *
 *  Smart-Node Reference Design Firmware 
 *  Hosted at git@pta-git1.csir.co.za:meraka-smart-campus/Smart-Node.git
 *  
 *  ASCII graphics: http://www.network-science.de/ascii/ , font = "big", width = 120
 *
 *  Libraries used: 
 *                  1. Install the Adafruit bme280 library using the Arduino library manager in the Arduino IDE
 *                  2. Install the Adafruit unified sensor abstraction layer library using the Arduino library manager in the Arduino IDE (https://github.com/adafruit/Adafruit_Sensor)
 *                  3. Install Christopher Laws' BH1750 library from https://github.com/claws/BH1750.  
 *  
 *  Notes: Sensors
 *                  1. BME280:  Absolute altitude measurements using the BME280 sensor can only be accurate if the hPa pressure at sea level for the location is known and set when the measurement is done
 *                              else it can be difficult to get more accurate than 10 meters.
 *                  
 *                  2. BH1730:  Lux sensor with I2C address = 0x23 (ADDR pin to Gnd), 0x5c  (ADDR pin to Vcc). The BH1730 is similar to the BH1750. References to BH1750 here imply BH1730.
 *                              Christopher Laws' library at (https://github.com/claws/BH1750) supports 6 modes of operation e.g BH1750_CONTINUOUS_LOW_RES_MODE
 *                              In continuous mode, sensor continuously measures lightness value. In one-time mode the sensor makes only one measurement and then goes into Power Down mode.
 *                              Continuuos and one-time modes each has three precisions:
 *                                - Low Resolution Mode - (4 lx precision, 16ms measurement time)
 *                                - High Resolution Mode - (1 lx precision, 120ms measurement time)
 *                                - High Resolution Mode 2 - (0.5 lx precision, 120ms measurement time)
 *                              Full mode list as defined in library:
 *                                BH1750_CONTINUOUS_LOW_RES_MODE
 *                                BH1750_CONTINUOUS_HIGH_RES_MODE (default)
 *                                BH1750_CONTINUOUS_HIGH_RES_MODE_2
 *                                BH1750_ONE_TIME_LOW_RES_MODE
 *                                BH1750_ONE_TIME_HIGH_RES_MODE
 *                                BH1750_ONE_TIME_HIGH_RES_MODE_2   
 *                              Code for this sensor is based on BH1750.ino, an example of BH1750 library usage.    
 *                              We initialise the BH1730 object using the default high resolution continuous mode and then makes a light level reading every second.   
 *                          
 *                  3. EKMC1603112 PIR sensor: The output is unpredictable during the initial 30 seconds after power up. Therefore, do not use the values sensed during this time. 
 *                              To be confirmed:  The output is high when motion is detected yet no holding time is specified in the specifications. 
 *                                It is therefore assumed that the output will immediately go low when no motion is detected => use rising edge to trigger ESP32 interrupt.
 *                                It is also assumed that the output is retriggerable, that is, the output will remain high as long as motion is detected. 
 *                                
 *                  4. BNO055 accelerometer, gyroscope, and magnetometer sensor: 
 *                                Based on BNO055_MS5637_ESP32 Basic Example Code available at https://github.com/kriswiner/ESP32/blob/master/Bosch/BNO055_MS5637.ino by Kris Winer
 *                                
 *          Actuators         
 *                    1. Mitsubishi airconditioner control is based on Smart_Node_Mitsubishi_IR_TX_27Feb2018.ino using ledcWrite function.
 *                    
 *          Reliability          
 *                                          
 *                    The ESP32 Arduino have I2C problems that surface from time to time and does not recover                        
 *                        Emperical evidence shows that, when this happens, often the values read on the I2C are 0xff  
 *                        Monitoring sysstat is a mechanism to detect this problem
 *                        
 *                    
 * Target board: 	ESP32_Dev_Module (ESP32_Core_Board_V2 printed on the PCB)              
 * 
 */


 

#include <Wire.h>                                               // required for BNO055 sensor and other I2C sensors
#include <WiFi.h>
#include <time.h>

#include <Adafruit_Sensor.h>                                    // required for the Bosch BME280 sensor
#include <Adafruit_BME280.h>                                    // required for the Bosch BME280 sensor

#include <BH1750.h>                                             // required for the BH1730 lux sensor. The BH1730 is similar to the BH1750





//  Program-wide device agnostic status flags

enum deviceStatusFlags {
   DEVICE_OK                = 0,
   DEVICE_INITIALIZED       = 1,
   DEVICE_NOT_INITIALIZED   = 16,
   DEVICE_INIT_ERROR        = 32,
   DEVICE_RUN_ERROR         = 64,
   UNKNOWN_ERROR            = 256  
};




// Configuration constants

static const unsigned long   REFRESH_SENSOR_INTERVAL       = 3000;        // ms
static const unsigned long   REFRESH_ACTUATOR_INTERVAL     = 30000;       // ms
static const unsigned long   REFRESH_INTERPRET_INTERVAL    = 1000;        // ms

static const int wifiFailTheshold = 5;                                    // connect attempt threshold above which we assume WiFi is not available

 
 
// Actuators and their associated pin numbers as printed on the ESP32 board

#define LED_PIN_RED          33      
#define LED_PIN_ORANGE       25    
#define LED_PIN_GREEN        26    
#define IR_LED_FRONT         32    
#define IR_LED_TOP           27     
#define BUZZER				       12




//
// Mitsubishi codes
// ----------------
//
//  We have four sequences available with which to control the Mitsubishi aircondioner.
//
//  The raw data in microseconds to be sent - required because the IR library does not decode the Mitsubishi codes
//  Data is in the following format: 
//                                       unsigned int raw[36]={"duration on","duration off",....};
//

uint16_t poweron[228]   = {3550,1600,400,1200,400,1200,400,400,400,400,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,1200,400,1200,400,400,400,400,400,1200,400,400,400,400,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,1200,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,1200,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,1200,400,400,400,400,400,400,400,400,400,1200,400};
uint16_t poweroff[228]  = {3400,1700,400,1200,400,1200,400,400,400,400,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,1200,400,1200,400,400,400,400,400,1200,400,400,400,400,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,1200,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,1200,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400};
uint16_t tempUp[228]    = {3500,1600,400,1200,400,1200,400,400,400,400,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,1200,400,1200,400,400,400,400,400,1200,400,400,400,400,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,1200,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,1200,400};
uint16_t tempDwn[228]   = {3500,1600,400,1200,400,1200,400,400,400,400,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,1200,400,1200,400,400,400,400,400,1200,400,400,400,400,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,400,400,1200,400,400,400,400,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,1200,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,1200,400,1200,400,1200,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,1200,400,400,400,1200,400,400,400,400,400,400,400,400,400,1200,400};



// 
//sensors
//



// BNO055 accelerometer sensor
#include "BNO055_defines.h"                               // BNO055 Register Map
#define BNO055_ADDRESS 0x29                               // I2C device address of BNO055 when ADO pin = 1

#define ADC_256  0x00                                     // define pressure and temperature conversion rates
#define ADC_512  0x02
#define ADC_1024 0x04
#define ADC_2048 0x06
#define ADC_4096 0x08
#define ADC_8192 0x0A
#define ADC_D1   0x40
#define ADC_D2   0x50





// BNO055 accelerometerinput parameters

enum Ascale {                                           // ACC Full Scale
  AFS_2G = 0,
  AFS_4G,
  AFS_8G,
  AFS_18G
};

enum Abw {                                              // ACC Bandwidth
  ABW_7_81Hz = 0,
  ABW_15_63Hz,
  ABW_31_25Hz,
  ABW_62_5Hz,
  ABW_125Hz,    
  ABW_250Hz,
  ABW_500Hz,     
  ABW_1000Hz,    //0x07
};

enum APwrMode {                                         // ACC Pwr Mode
  NormalA = 0,  
  SuspendA,
  LowPower1A,
  StandbyA,        
  LowPower2A,
  DeepSuspendA
};

enum Gscale {                                           // gyro full scale
  GFS_2000DPS = 0,
  GFS_1000DPS,
  GFS_500DPS,
  GFS_250DPS,
  GFS_125DPS                                            // 0x04
};

enum GPwrMode {                                         // GYR Pwr Mode
  NormalG = 0,
  FastPowerUpG,
  DeepSuspendedG,
  SuspendG,
  AdvancedPowerSaveG
};

enum Gbw {                                              // gyro bandwidth
  GBW_523Hz = 0,
  GBW_230Hz,
  GBW_116Hz,
  GBW_47Hz,
  GBW_23Hz,
  GBW_12Hz,
  GBW_64Hz,
  GBW_32Hz
};

enum OPRMode {                                          // BNO-55 operation modes
  CONFIGMODE = 0x00,
                                                        // Sensor Mode
  ACCONLY,
  MAGONLY,
  GYROONLY,
  ACCMAG,
  ACCGYRO,
  MAGGYRO,
  AMG,                                                  // 0x07
                                                        // Fusion Mode
  IMU,
  COMPASS,
  M4G,
  NDOF_FMC_OFF,
  NDOF                                                  // 0x0C
};

enum PWRMode {
  Normalpwr = 0,   
  Lowpower,       
  Suspendpwr       
};

enum Modr {                                           // magnetometer output data rate  
  MODR_2Hz = 0,     
  MODR_6Hz,
  MODR_8Hz,
  MODR_10Hz,  
  MODR_15Hz,
  MODR_20Hz,
  MODR_25Hz, 
  MODR_30Hz 
};

enum MOpMode {                                        // MAG Op Mode
  LowPower = 0,
  Regular,
  EnhancedRegular,
  HighAccuracy
};

enum MPwrMode {                                       // MAG power mode
  Normal = 0,   
  Sleep,     
  Suspend,
  ForceMode  
};


// Specify BNO055 sensor configuration

uint8_t OSR = ADC_8192;                                                     // set pressure and temperature oversample rate

uint8_t GPwrMode = NormalG;                                                 // Gyro power mode
uint8_t Gscale = GFS_250DPS;                                                // Gyro full scale
                                                                            // uint8_t Godr = GODR_250Hz;    // Gyro sample rate
uint8_t Gbw = GBW_23Hz;                                                     // Gyro bandwidth

uint8_t Ascale = AFS_2G;                                                    // Accel full scale
                                                                            // uint8_t Aodr = AODR_250Hz;    // Accel sample rate
uint8_t APwrMode = NormalA;                                                 // Accel power mode
uint8_t Abw = ABW_31_25Hz;                                                  // Accel bandwidth, accel sample rate divided by ABW_divx

                                                                            // uint8_t Mscale = MFS_4Gauss;// Select magnetometer full-scale resolution
uint8_t MOpMode = Regular;                                                  // Select magnetometer perfomance mode
uint8_t MPwrMode = Normal;                                                  // Select magnetometer power mode
uint8_t Modr = MODR_10Hz;                                                   // Select magnetometer ODR when in BNO055 bypass mode

uint8_t PWRMode = Normalpwr;                                                // Select BNO055 power mode
uint8_t OPRMode = NDOF;                                                     // specify operation mode for sensors
uint8_t status;                                                             // BNO055 data status register
float aRes, gRes, mRes;                                                     // scale resolutions per LSB for the sensors

// BNO055 Pin definitions

int BNO055intPin = 14;                                                      // These can be changed, 2 and 3 are the Arduinos ext int pins
int BNO055ResetPin = 2;                                                     // BNO055 Reset Pin, active low



// ---- some of the below are redundant and can be deleted -----------

unsigned char nCRC;                                             // calculated check sum to ensure PROM integrity

uint32_t D1 = 0, D2 = 0;                                        // raw MS5637 pressure and temperature data
double dT, OFFSET, SENS, TT2, OFFSET2, SENS2;                   // First order and second order corrections for raw S5637 temperature and pressure data

int16_t accelCount[3];                                          // Stores the 16-bit signed accelerometer sensor output
int16_t gyroCount[3];                                           // Stores the 16-bit signed gyro sensor output
int16_t magCount[3];                                            // Stores the 16-bit signed magnetometer sensor output
int16_t quatCount[4];                                           // Stores the 16-bit signed quaternion output
int16_t EulCount[3];                                            // Stores the 16-bit signed Euler angle output
int16_t LIACount[3];                                            // Stores the 16-bit signed linear acceleration output
int16_t GRVCount[3];                                            // Stores the 16-bit signed gravity vector output
float gyroBias[3] = {0, 0, 0}, accelBias[3] = {0, 0, 0}, magBias[3] = {0, 0, 0};  // Bias corrections for gyro, accelerometer, and magnetometer
int16_t tempGCount;                                             // temperature raw count output of gyro
float   Gtemperature;                                           // Stores the BNO055 gyro internal chip temperatures in degrees Celsius
double Temperature, Pressure;                                   // stores MS5637 pressures sensor pressure and temperature

                                                                // global constants for 9 DoF fusion and AHRS (Attitude and Heading Reference System)
float GyroMeasError = PI * (40.0f / 180.0f);                    // gyroscope measurement error in rads/s (start at 40 deg/s)
float GyroMeasDrift = PI * (0.0f  / 180.0f);                    // gyroscope measurement drift in rad/s/s (start at 0.0 deg/s/s)
                                                                // There is a tradeoff in the beta parameter between accuracy and response speed.
                                                                // In the original Madgwick study, beta of 0.041 (corresponding to GyroMeasError of 2.7 degrees/s) was found to give optimal accuracy.
                                                                // However, with this value, the BNO055 response time is about 10 seconds to a stable initial quaternion.
                                                                // Subsequent changes also require a longish lag time to a stable output, not fast enough for a quadcopter or robot car!
                                                                // By increasing beta (GyroMeasError) by about a factor of fifteen, the response time constant is reduced to ~2 sec
                                                                // I haven't noticed any reduction in solution accuracy. This is essentially the I coefficient in a PID control sense; 
                                                                // the bigger the feedback coefficient, the faster the solution converges, usually at the expense of accuracy. 
                                                                // In any case, this is the free parameter in the Madgwick filtering and fusion scheme.
float beta = sqrt(3.0f / 4.0f) * GyroMeasError;                 // compute beta
float zeta = sqrt(3.0f / 4.0f) * GyroMeasDrift;                 // compute zeta, the other free parameter in the Madgwick scheme usually set to a small or zero value

#define Kp 2.0f * 5.0f                                          // these are the free parameters in the Mahony filter and fusion scheme, Kp for proportional feedback, Ki for integral
#define Ki 0.0f


uint32_t delt_t = 0, count = 0, sumCount = 0;                   // used to control display output rate (ACS: do we need this?)

float pitch, yaw, roll;
float Pitch, Yaw, Roll;
float LIAx, LIAy, LIAz, GRVx, GRVy, GRVz;
float deltat = 0.0f, sum = 0.0f;                                // integration interval for both filter schemes
uint32_t lastUpdate = 0, firstUpdate = 0;                       // used to calculate integration interval
uint32_t Now = 0;                                               // used to calculate integration interval

float ax, ay, az, gx, gy, gz, mx, my, mz;                       // variables to hold latest sensor data values 
float q[4] = {1.0f, 0.0f, 0.0f, 0.0f};                          // vector to hold quaternion
float quat[4] = {1.0f, 0.0f, 0.0f, 0.0f};                       // vector to hold quaternion
float eInt[3] = {0.0f, 0.0f, 0.0f};                             // vector to hold integral error for Mahony method







// BME280 temperature, humidity, and pressure sensor            // I2C address assumed to be the library default of 0x77 as defined in Adafruit_BME280.h
#define SEALEVELPRESSURE_HPA (1013.25)                          // Used with the Bosch BME280 sensor to calculate altitude
uint16_t lux = 1010;                                            // Initialise to an unlikely default value to help with debugging if required
deviceStatusFlags BME280Status = DEVICE_NOT_INITIALIZED;



// BH1730 Lux sensor 
deviceStatusFlags BH1730Status = DEVICE_NOT_INITIALIZED;




// EKMC1603112 PIR sensor
const byte    EKMC1603112interruptPin      = 34;
volatile int  EKMC1603112interruptCounter  = 0;                 // Communicate with the main loop function and signal that an interrupt has occurred.    
                                                                //  This variable will be changed in the ISR, and Read in main loop
                                                                //  Volatile since it will be shared by the ISR and the main code. Otherwise, it may be removed due to compiler optimizations.
                                                                //  Volatile: tells the compiler that this variables value must not be stored in a CPU register, it must exist
                                                                //    in memory at all times.  This means that every time the value must be read or
                                                                //    changed, it has be read from memory, updated, stored back into RAM, that way, when the ISR 
                                                                //    is triggered, the current value is in RAM.  Otherwise, the compiler will find ways to increase efficiency
                                                                //    of access.  One of these methods is to store it in a CPU register, but if it does that,(keeps the current
                                                                //    value in a register, when the interrupt triggers, the Interrupt access the 'old' value stored in RAM, 
                                                                //    changes it, then returns to whatever part of the program it interrupted.  Because the foreground task,
                                                                //    (the one that was interrupted) has no idea the RAM value has changed, it uses the value it 'know' is 
                                                                //    correct (the one in the register).  [https://github.com/espressif/arduino-esp32/issues/855]
                                                                
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;                // Take care of the synchronization between the main code and the EKMC1603112 PIR interrupt to prevent concurrent access problems.



                                              
//
//  PWM
//

#define TOP_LEDC_CHANNEL     0                                  // Use first channel of 16 channels (started from zero) - for TOP IR LED
#define FRONT_LEDC_CHANNEL   1                                  // Use second channel of 16 channels (started from zero) - for FRONT IR LED





//
//  I2C addresses
//

#define address_bh1730 0x23                                     // BH1750's I2C address is 0x23 if addr pin is low, else 0x5C if addr pin is pulled high




// 
//  Wireless interface
//

//static const char *          WIRELESS_SSID = "CSIR Guest";        // As a test when "smart_campus" is down
static const char *          WIRELESS_SSID = "smart_campus";        // CSIR smart_campus WLAN uses ACL (access control list)with pre-enrolled MACs, no password required
static const char *          WIRELESS_PASSWORD = "none_required";

byte ESP32_mac[6];                                                  // The ESP32 MAC

deviceStatusFlags WiFiStatus = DEVICE_NOT_INITIALIZED;


//
//  NTP Server Details
//

static const char *          NTP_SERVER = "ntp1.meraka.csir.co.za";
static const long int        GMT_OFFSET = +7200;




// 
//  Kura Server Details
//

static const char *          KAPUA_SERVER = "www.google.co.za";
static const int             KAPUA_PORT = 80;





//
// instantiate sensors
//

Adafruit_BME280   bme280;                                       // BME280: I2C using the default I2C bus, no pins are assigned

BH1750 BH1730;                                                  // Create a BH1730 instance based on the BH1750 class. The BH1730 is similar to the BH1750





  
void loop() {

  static unsigned long lastSensorRefreshTime      = millis();
  static unsigned long lastActuatorRefreshTime    = millis();
  static unsigned long lastInterpretTime          = millis();

  //    
  // Service routines for immediate execution
  //

  // <blank>



  
  //
  // Service routines for sensor updates
  //
  
  if(millis() - lastSensorRefreshTime >= REFRESH_SENSOR_INTERVAL)               // is it time to update sensors values?
  {
    printNTPTime();
    lastSensorRefreshTime += REFRESH_SENSOR_INTERVAL;
    update_sensors();
    mqtt();
//Serial.print(bme280.readTemperature());             Serial.println(" [degrees C]   "); 
  } 



  

  //
  // Service routines to interpret sensor values
  //
//   if(millis() - lastInterpretTime >= REFRESH_INTERPRET_INTERVAL)               // is it time to interpret the sensors values?
//  {
//    lastInterpretTime += REFRESH_INTERPRET_INTERVAL;
//    
//    if ( bme280.readTemperature()< 0 ) {                                        // todo: replace with better test condition
//      forceReboot();
//    }
//    
//  }  




  //
  // Service routines for actuator updates 
  //
  if(millis() - lastActuatorRefreshTime >= REFRESH_ACTUATOR_INTERVAL)           // is it time to update the actuators?
  {
    //printNTPTime();
    lastActuatorRefreshTime += REFRESH_ACTUATOR_INTERVAL;

    //update_actuators();
  } 

  
  
}






void activate_actuators_once(){          // use for visual and auditory test of actuators
  
  //turn ON the leds
  digitalWrite(LED_PIN_RED,     HIGH);   // turn the LED on
  delay(50);
  digitalWrite(LED_PIN_ORANGE,  HIGH);   // turn the LED on  
  delay(50);
  digitalWrite(LED_PIN_GREEN,   HIGH);   // turn the LED on
  delay(50);
  digitalWrite(IR_LED_FRONT,    HIGH);   // turn the LED on  
  digitalWrite(IR_LED_TOP,      HIGH);   // turn the LED on
  delay(50);
  digitalWrite(BUZZER,          HIGH);   // turn the buzzer on
  delay(10);

  //turn OFF the leds
  digitalWrite(LED_PIN_RED,     LOW);   // turn the LED off
  digitalWrite(LED_PIN_ORANGE,  LOW);   // turn the LED off  
  digitalWrite(LED_PIN_GREEN,   LOW);   // turn the LED off
  digitalWrite(IR_LED_FRONT,    LOW);   // turn the LED off  
  digitalWrite(IR_LED_TOP,      LOW);   // turn the LED off
  digitalWrite(BUZZER,          LOW);   // turn the buzzer off
  delay(20);
  
} 

// something is not working - force a reboot
void forceReboot() {
      Serial.println();
      Serial.println("=================================== restarting ESP32  ==================================");
      activate_actuators_once();  
      delay(400);
      Serial.print("Countdown: 2");
      activate_actuators_once();   
      delay(250);
      Serial.print("Countdown: 1");
      activate_actuators_once();   
      Serial.print("Countdown: 0");
      ESP.restart();                                            // Note: Software reset does not reset peripherals like I2C "Peripherals (except for WiFi, BT, UART0, SPI1, and legacy timers) are not reset."
      //ESP.esp_restart_noos();                                 // Not available? Gives compiler error. <https://www.esp32.com/viewtopic.php?t=4118> 
}


// Indicate that something is wrong by using the LEDs and buzzer
void indicateErrorCondition() {
  digitalWrite(LED_PIN_RED,     HIGH);   // turn the LED on
  for (int i = 0; i<10; i++) {
    Serial.print("! ");
    digitalWrite(BUZZER,          HIGH);   // turn the buzzer on
    delay(20);  
    digitalWrite(BUZZER,          LOW);   // turn the buzzer off
    delay(10);
  }
  delay(5000);
  digitalWrite(LED_PIN_RED,     LOW);   // turn the LED off 
}



void printNTPTime(){ 
   struct tm timeinfo;
    if(!getLocalTime(&timeinfo)){
        indicateErrorCondition();
        Serial.println("ERROR [void printNTPTime()]: Failed to obtain time from NTP server");
        return;
    }
    
   Serial.print("NTP time: "); Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");   
}

