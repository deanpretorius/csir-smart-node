void setup() {
  serial_setup();
   
  Serial.println(); Serial.println();  Serial.println();
  Serial.println("@@@@@@@@@@@@@@@@@@@@"); 
  Serial.println("@                  @");
  Serial.println("@ Setup: initiated @"); 
  Serial.println("@                  @");
  Serial.println("@@@@@@@@@@@@@@@@@@@@");

  actuators_setup();
  indicateErrorCondition();                                     // another check to verify that Buzzer and RED LED work  
  i2c_setup(); 
  wifi_setup();
  sensor_setup();
  ntp_setup();
  kapua_setup();
  mqtt_setup();
  delay(5000);
  Serial.println("@@@@@@@@@@@@@@@@@@@@"); 
  Serial.println("@                  @"); 
  Serial.println("@ Setup: completed @");
  Serial.println("@                  @");
  Serial.println("@@@@@@@@@@@@@@@@@@@@");  
  Serial.println();Serial.println(); Serial.println();
}




void i2c_setup(){

   //Wire.begin();                                              // Initialize the I2C bus (the BH1750 library doesn't do this automatically)  
   Wire.begin(21, 22, 400000);                                  // [SDA, SCL] = [21, 22] are default on ESP32, 400 kHz I2C bus speed
                                                                // I2C devices can be diverted to the new pins, eg. on esp8266 devices you can select SCL and SDA pins using Wire.begin(D4, D3);
   Serial.println();
   Serial.println("I2C configured");
    
}



void serial_setup(){

  Serial.begin(115200);
  while(!Serial);                                               // wait for the serial port to open
  Serial.println("Serial port initialised");
    
}



void actuators_setup(){

  pinMode(LED_PIN_RED,      OUTPUT);
  pinMode(LED_PIN_ORANGE,   OUTPUT);
  pinMode(LED_PIN_GREEN,    OUTPUT);
  pinMode(IR_LED_FRONT,     OUTPUT);
  pinMode(IR_LED_TOP,       OUTPUT);
  pinMode(BUZZER,           OUTPUT);
  
  activate_actuators_once();                                    // test if the actuators are operational

  ledcSetup(TOP_LEDC_CHANNEL, 36000, 3);                        //(channel, frequency, timer resolution)

  ledcAttachPin(IR_LED_TOP,     TOP_LEDC_CHANNEL);              //connect IR LED to PWM
  ledcAttachPin(IR_LED_FRONT,   FRONT_LEDC_CHANNEL);            //connect IR LED to PWM
  Serial.println();
  Serial.println("Actuators configured");

}



void sensor_setup(){

  //BME XXXX SETUP
  //etc

 Serial.println("!!!! Debugging I2C problem. DOn't initialise or read BNO055 as debug attempt"); 
 
//    Serial.println();
//    Serial.println    ("+========+");
//    Serial.println    ("| BNO055 |");
//    Serial.println    ("+========+ ");
//    Serial.println();
//    
//  // initialise the BNO055
//  pinMode(BNO055intPin, INPUT);                                                                               // Set up the BNO055 interrupt pin, its set as active high, push-pull
//  
//  pinMode(BNO055ResetPin, OUTPUT);                                                                            // Enable the BNO055 by pulling its reset pin high
//  digitalWrite(BNO055ResetPin, HIGH);
//
//  Serial.println();
//  Serial.println(" Waiting while the BNO055 starts up...");                                           
//  waitBNO055();                                                                                               // Ref: App note, p4 - the BNO055 needs time to start up
//  Serial.println();
//  
//  // Read the WHO_AM_I register of [BNO055], this is a good test of communication
//  Serial.println("Checking the BNO055 9-axis motion sensor");
//  Serial.println("----------------------------------------");
//  Serial.println();
//  Serial.println();
//  
//  byte c = readByte(BNO055_ADDRESS, BNO055_CHIP_ID);                                                          // Read WHO_AM_I register for BNO055
//  Serial.print("BNO055 Address assumed to be = 0x"); Serial.println(BNO055_ADDRESS, HEX);
//  Serial.print("BNO055 identifies itself as: "); Serial.print("I AM 0x"); Serial.print(c, HEX); Serial.print(" (should be 0xA0)");  Serial.println();
//
//  delay(1000); 
//
//  Serial.println("Reading the WHO_AM_I register of the [accelerometer]");                                     // Read the WHO_AM_I register of the [accelerometer], this is a good test of communication
//  byte d = readByte(BNO055_ADDRESS, BNO055_ACC_ID);                                                           // Read WHO_AM_I register for accelerometer
//  Serial.print("BNO055 ACC says: "); Serial.print("I AM 0x"); Serial.print(d, HEX); Serial.print(" (should be 0xFB)"); Serial.println();
//
//  delay(1000); 
//  
//  Serial.println("Reading the WHO_AM_I register of the [magnetometer]");                                      // Read the WHO_AM_I register of the [magnetometer], this is a good test of communication
//  byte e = readByte(BNO055_ADDRESS, BNO055_MAG_ID);                                                           // Read WHO_AM_I register for magnetometer
//  Serial.print("BNO055 MAG says: "); Serial.print("I AM 0x"); Serial.print(e, HEX); Serial.print(" (should be 0x32)"); Serial.println();
//
//  delay(1000);   
//   
//  Serial.println("Read the WHO_AM_I register of the [gyroscope]");                                            // Read the WHO_AM_I register of the [gyroscope], this is a good test of communication
//  byte f = readByte(BNO055_ADDRESS, BNO055_GYRO_ID);                                                          // Read WHO_AM_I register for gyroscope
//  Serial.print("BNO055 GYRO says: "); Serial.print("I AM 0x"); Serial.print(f, HEX); Serial.print(" (should be 0x0F)"); Serial.println();
//
//  delay(1000); 
//
//  if (c == 0xA0)                                                                                              // BNO055 WHO_AM_I should always be 0xA0
//  {  
//    Serial.println("BNO055 is online...");
//    
//    // Check software revision ID
//    byte swlsb = readByte(BNO055_ADDRESS, BNO055_SW_REV_ID_LSB);
//    byte swmsb = readByte(BNO055_ADDRESS, BNO055_SW_REV_ID_MSB);
//    Serial.print("BNO055 SW Revision ID: "); Serial.print(swmsb, HEX); Serial.print("."); Serial.print(swlsb, HEX); Serial.println(" (Should be 03.04 or higher)");
//    
//    
//    byte blid = readByte(BNO055_ADDRESS, BNO055_BL_REV_ID);                                                   // Check bootloader version
//    Serial.print("BNO055 bootloader Version: "); Serial.println(blid); 
//
//    Serial.println();
//    Serial.println    ("+-------------------+");
//    Serial.println    ("| BNO055 self tests |");
//    Serial.println    ("+-------------------+ ");
//    Serial.println();
//    
//    //
//    //  Check BNO055 self-test results
//    //
//    byte selftest = readByte(BNO055_ADDRESS, BNO055_ST_RESULT); 
//    if(selftest & 0x01) {
//      Serial.println("BNO055 PASSED: accelerometer selftest"); 
//    } else {
//      indicateErrorCondition();
//      Serial.println("BNO055 ERROR: accelerometer -FAILED- selftest"); 
//    }
//    if(selftest & 0x02) {
//      Serial.println("BNO055 PASSED: magnetometer selftest"); 
//    } else {
//      Serial.println("BNO055 ERROR: magnetometer -FAILED- selftest"); 
//    }  
//    if(selftest & 0x04) {
//      Serial.println("BNO055 PASSED: gyroscope selftest"); 
//    } else {
//      indicateErrorCondition();
//      Serial.println("BNO055 ERROR: gyroscope -FAILED- selftest"); 
//    }      
//    if(selftest & 0x08) {
//      Serial.println("BNO055 PASSED: MCU selftest"); 
//    } else {
//      indicateErrorCondition();
//      Serial.println("BNO055 ERROR: MCU -FAILED- selftest"); 
//    }
//      
//  delay(1000);
//  
//  accelgyroCalBNO055(accelBias, gyroBias);
//
//  Serial.println("BNO055 biases (accelerometer and gyro)");
//  Serial.println("--------------------------------------");
//  Serial.print("Average accelerometer bias (mg) = ");   Serial.print(accelBias[0]); Serial.print("   "); Serial.print(accelBias[1]); Serial.print("   "); Serial.println(accelBias[2]);
//  Serial.print("Average gyro bias (dps)         = ");   Serial.print(gyroBias[0]);  Serial.print("   ");  Serial.print(gyroBias[1]); Serial.print("   "); Serial.println(gyroBias[2]);
//  
//  delay(1000); 
//  
//  magCalBNO055(magBias);
//
//  Serial.println("BNO055 biases (magnetometer)");
//  Serial.println("----------------------------");
//  Serial.println("Average magnetometer bias (mG) = "); Serial.print(magBias[0]);  Serial.print("   "); Serial.print(magBias[1]);  Serial.print("   "); Serial.println(magBias[2]);
// 
//  delay(1000);  
//                                                                                                    // Check calibration status of the sensors
//  uint8_t calstat = readByte(BNO055_ADDRESS, BNO055_CALIB_STAT);
//  Serial.println("");
//  Serial.println("BNO055 calibrartion states             (Interpret as follows: 0 => Not calibrated, 3 => fully calibrated)");
//  Serial.println("--------------------------");
// 
//  Serial.print("System calibration status "); Serial.println( (0xC0 & calstat) >> 6);
//  Serial.print("Gyro   calibration status "); Serial.println( (0x30 & calstat) >> 4);
//  Serial.print("Accel  calibration status "); Serial.println( (0x0C & calstat) >> 2);
//  Serial.print("Mag    calibration status "); Serial.println( (0x03 & calstat) >> 0);
//  
//  initBNO055();                                                                                     // Initialize the BNO055
//  Serial.println("BNO055 initialized for sensor mode....");                                         // Initialize BNO055 for sensor read 
// 
//  } //  if (c == 0xA0)
//  else                                                                                              // BNO055 did not respond to I2C query
//  {
//    Serial.print("BNO055 ERROR: Could not connect to BNO055: 0x");
//    Serial.println(c, HEX);
//    indicateErrorCondition();
//    forceReboot();                                                                                     // reboot
//  } // if (c == 0xA0)
//
//  

  //
  // initialise BME280 temperature, humidity, and pressure
  //

  
    Serial.println();
    Serial.println    ("+========+");
    Serial.println    ("| BME280 |");
    Serial.println    ("+========+ ");
    Serial.println();
    
  if (!bme280.begin()) {                                                                            // will return True if the sensor was found
    Serial.println("BME280 ERROR: Initialisation error - Could not find a valid BME280 sensor.");
    indicateErrorCondition();
    BME280Status = DEVICE_INIT_ERROR;
  } else {
    Serial.println(F("BME280 initialised"));
    BME280Status = DEVICE_OK;
  }

    

  //
  //initialise BH1730 Lux sensor
  //        
  
    Serial.println();
    Serial.println    ("+========+");
    Serial.println    ("| BH1730 |");
    Serial.println    ("+========+ ");
    Serial.println();
    
   if (BH1730.begin(BH1750::CONTINUOUS_HIGH_RES_MODE)) {
    Serial.println(F("BH1730 initialised in CONTINUOUS_HIGH_RES_MODE"));
    BH1730Status = DEVICE_OK;
  }
  else {
    Serial.println(F("Initialisation error: BH1730"));
    indicateErrorCondition();
    BH1730Status = DEVICE_INIT_ERROR;
  } 





  //
  // initialise EKMC1603112 (PIR sensor)
  //                                                                                                //configure interrupt on ESP32 pin IO34
  
  Serial.println();
  Serial.println    ("+=================+");
  Serial.println    ("| EKMC1603112 PIR |");
  Serial.println    ("+=================+ ");
  Serial.println();
  
  Serial.println("EKMC1603112 PIR: Configuring interrupt");
  pinMode(EKMC1603112interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(EKMC1603112interruptPin), EKMC1603112handleInterrupt, RISING);
  Serial.println(F("EKMC1603112: interrupt configured"));



  Serial.println("Sensors: Configured");
}





void wifi_setup(){

  Serial.println();
  Serial.println    ("+======+");
  Serial.println    ("| WiFi |");
  Serial.println    ("+======+ ");
  Serial.println();
  
  Serial.print("WiFi: connecting to WiFi network: " + String(WIRELESS_SSID) + "   progress (6=WL_CONNECT_FAILED):  ");
  WiFi.begin(WIRELESS_SSID,WIRELESS_PASSWORD);
  
  delay(500);  

                                                                                        // make a limited number of attempts to connect to the WiFi AP
  int wifiFailCounter = 0;
  while ( (WiFi.status() != WL_CONNECTED) && (wifiFailCounter < wifiFailTheshold) ) {
    Serial.print(WiFi.status());
    delay(1000);
    wifiFailCounter++;
  }
  
  if ( wifiFailCounter >= wifiFailTheshold)   {                                         // WiFi has a problem
    indicateErrorCondition();
    Serial.println(); Serial.print("ERROR_WiFi: WiFi not connected. ");
    WiFiStatus = DEVICE_INIT_ERROR;
    delay(5000);
    forceReboot();
  } else {                                                                              // WiFi is OK
    WiFiStatus = DEVICE_OK;
    Serial.println(); Serial.print("WiFi: connected. "); Serial.print("              IP address = "); Serial.print(WiFi.localIP());   
  }
  
  WiFi.macAddress(ESP32_mac);
  
  Serial.print("             ESP32 radio MAC = ");
  Serial.print(ESP32_mac[0],HEX);
  Serial.print(":");
  Serial.print(ESP32_mac[1],HEX);
  Serial.print(":");
  Serial.print(ESP32_mac[2],HEX);
  Serial.print(":");
  Serial.print(ESP32_mac[3],HEX);
  Serial.print(":");
  Serial.print(ESP32_mac[4],HEX);
  Serial.print(":");
  Serial.println(ESP32_mac[5],HEX);

}




void ntp_setup(){

  Serial.println();
  Serial.println    ("+======+");
  Serial.println    ("| NTP  |");
  Serial.println    ("+======+ ");
  Serial.println();
  
  configTime(GMT_OFFSET, 0, NTP_SERVER);
   
   struct tm timeinfo;
    if(!getLocalTime(&timeinfo)){
        indicateErrorCondition();
        Serial.println("Failed to obtain time");
        return;
    }
    
   Serial.print("NTP time updated: "); Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");   
}




void kapua_setup(){

  Serial.println();
  Serial.println    ("+=======+");
  Serial.println    ("| Kapua |");
  Serial.println    ("+=======+ ");
  Serial.println();
  
  Serial.println("Kapua: not implemented - no connection created");
    
}

