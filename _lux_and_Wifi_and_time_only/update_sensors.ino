
void update_sensors() {



//  Serial.println();
//  Serial.println("----------------------------------------------------");
//  Serial.println("-------------- Updating Sensor values --------------");
//  Serial.println("----------------------------------------------------");
  digitalWrite(LED_PIN_GREEN,  HIGH);  delay(4);  digitalWrite(LED_PIN_GREEN,  LOW);   
    
 Serial.print("MCU time (seconds) = "); Serial.println(millis()/1000);      



  
  //    
  // Place code to read and update sensors here.
  //

 //Serial.println("!!!! Debugging I2C problem. Therefore this code not reading BNO055 sensor by design. Outcome : I2C problem manifests after ~11600 seconds.");


//
//
//                                // BNO055 sensor
//                                //
//                                //
//                                //
//  
//  readAccelData(accelCount);                                                                                      // Read the x/y/z adc values
//                                                                                                                  // Now we calculate the accleration value into actual mg's
//  ax = (float)accelCount[0]; // - accelBias[0];                                                                   // subtract off calculated accel bias
//  ay = (float)accelCount[1]; // - accelBias[1];
//  az = (float)accelCount[2]; // - accelBias[2]; 
//
//  readGyroData(gyroCount);                                                                                        // Read the x/y/z adc values
//                                                                                                                  // Calculate the gyro value into actual degrees per second
//  gx = (float)gyroCount[0]/16.; // - gyroBias[0];                                                                 // subtract off calculated gyro bias
//  gy = (float)gyroCount[1]/16.; // - gyroBias[1];  
//  gz = (float)gyroCount[2]/16.; // - gyroBias[2];   
//
//  readMagData(magCount);                                                                                          // Read the x/y/z adc values   
//                                                                                                                  // Calculate the magnetometer values in milliGauss
//  mx = (float)magCount[0]/1.6; // - magBias[0];                                                                   // get actual magnetometer value in mGauss 
//  my = (float)magCount[1]/1.6; // - magBias[1];  
//  mz = (float)magCount[2]/1.6; // - magBias[2];   
//  
//  readQuatData(quatCount);                                                                                        // Read the x/y/z adc values   
//                                                                                                                  // Calculate the quaternion values  
//  quat[0] = (float)(quatCount[0])/16384.;    
//  quat[1] = (float)(quatCount[1])/16384.;  
//  quat[2] = (float)(quatCount[2])/16384.;   
//  quat[3] = (float)(quatCount[3])/16384.;   
//  
//  readEulData(EulCount);                                                                                          // Read the x/y/z adc values   
//                                                                                                                  // Calculate the Euler angles values in degrees
//  Yaw = (float)EulCount[0]/16.;  
//  Roll = (float)EulCount[1]/16.;  
//  Pitch = (float)EulCount[2]/16.;   
//
//  readLIAData(LIACount);                                                                                          // Read the x/y/z adc values   
//                                                                                                                  // Calculate the linear acceleration (sans gravity) values in mg
//  LIAx = (float)LIACount[0];  
//  LIAy = (float)LIACount[1];  
//  LIAz = (float)LIACount[2];   
//
//  readGRVData(GRVCount);                                                                                          // Read the x/y/z adc values   
//                                                                                                                  // Calculate the linear acceleration (sans gravity) values in mg
//  GRVx = (float)GRVCount[0];  
//  GRVy = (float)GRVCount[1];  
//  GRVz = (float)GRVCount[2];   
//    
//  Now = micros();
//  deltat = ((Now - lastUpdate)/1000000.0f);                                                                       // set integration time by time elapsed since last filter update
//  lastUpdate = Now;
//  
//  sum += deltat;                                                                                                  // sum for averaging filter update rate
//  sumCount++;
//  
//  // Sensors x, y, and z-axes  for the three sensor: accel, gyro, and magnetometer are all aligned, so
//  // no allowance for any orientation mismatch in feeding the output to the quaternion filter is required.
//  // For the BNO055, the sensor forward is along the x-axis just like
//  // in the LSM9DS0 and MPU9250 sensors. This rotation can be modified to allow any convenient orientation convention.
//  // This is ok by aircraft orientation standards!  
//  // Pass gyro rate as rad/s
//  
//                                                                                                                  // check BNO055 error status 
//  Serial.println(""); Serial.println("");
//  Serial.println(" BNO055 UPDATED readings ");
//  Serial.println("=========================");
//  Serial.println();
//  
//  uint8_t sysstat = readByte(BNO055_ADDRESS, BNO055_SYS_STATUS);                                                  // check system status
//    
//  Serial.print("BNO055 system status (higher is better: 0=discard readings, 5=Sensor Fusion running) = 0x"); Serial.println(sysstat, HEX);
//
//  if (sysstat == 0xFF) {                                                                                          // The ESP32 Arduino have I2C problems that surface from time to time and does not recover
//    Serial.print("ERROR indicated on I2C bus.");                                                                  // Emperical evidence shows that, when this happens, often the values read on the I2C are 0xff  
//    indicateErrorCondition();                                                                                     // Monitoring sysstat is a mechanism to detect this problem.
//    forceReboot();                                                                        
//  } 
//
//  if(sysstat == 0x05) Serial.println("Sensor fusion algorithm running");
//  if(sysstat == 0x06) Serial.println("ERROR: Sensor fusion algorithm NOT running");
//  if(sysstat == 0x01) {
//    uint8_t syserr = readByte(BNO055_ADDRESS, BNO055_SYS_ERR);
//    if(syserr == 0x01) Serial.println("BNO055 ERROR: Peripheral initialization error");
//    if(syserr == 0x02) Serial.println("BNO055 ERROR: System initialization error");
//    if(syserr == 0x03) Serial.println("BNO055 ERROR: Self test result failed");
//    if(syserr == 0x04) Serial.println("BNO055 ERROR: Register map value out of range");
//    if(syserr == 0x05) Serial.println("BNO055 ERROR: Register map address out of range");
//    if(syserr == 0x06) Serial.println("BNO055 ERROR: Register map write error");
//    if(syserr == 0x07) Serial.println("BNO055 ERROR: BNO low power mode no available for selected operation mode");
//    if(syserr == 0x08) Serial.println("BNO055 ERROR: Accelerometer power mode not available");
//    if(syserr == 0x09) Serial.println("BNO055 ERROR: Fusion algorithm configuration error");
//    if(syserr == 0x0A) Serial.println("BNO055 ERROR: Sensor configuration error");    
//  }       // if(sysstat == 0x01)
//
//  tempGCount = readGyroTempData();                                                                                // Read the gyro adc values
//  Gtemperature = (float) tempGCount;                                                                              // Gyro chip temperature in degrees Centigrade
//  Serial.print("Gyro temperature = ");  Serial.print(Gtemperature, 1);  Serial.println(" degrees C");            // Print gyro die temperature in degrees Centigrade      
//                                                                                                                  // Print T values to tenths of a degree C
//  Serial.println("");
//  Serial.print("[   ax  =  ");         Serial.print( (int)ax );  
//  Serial.print("    ay  = ");       Serial.print( (int)ay ); 
//  Serial.print("    az  = ");       Serial.print( (int)az );  Serial.println("  (mg)]");
//  Serial.print("[   gx = ");           Serial.print( gx, 2); 
//  Serial.print("   gy = ");         Serial.print( gy, 2); 
//  Serial.print("   gz = ");         Serial.print( gz, 2);     Serial.println("  (deg/s)]");
//  Serial.print("[   mx = ");           Serial.print( (int)mx ); 
//  Serial.print("   my = ");         Serial.print( (int)my ); 
//  Serial.print("   mz = ");         Serial.print( (int)mz );  Serial.println("  (mG)]");
//  
//  Serial.print("[   qx = ");           Serial.print( q[0] );
//  Serial.print("   qy =  ");        Serial.print( q[1] ); 
//  Serial.print("   qz = ");         Serial.print( q[2] ); 
//  Serial.print("   qw = ");         Serial.print( q[3] );     Serial.println("   ]");
//  Serial.print("[   quatw  = ");       Serial.print( quat[0] );    
//  Serial.print("   quatx = ");      Serial.print( quat[1] ); 
//  Serial.print("   quaty = ");      Serial.print( quat[2] ); 
//  Serial.print("   quatz = ");      Serial.print( quat[3] );    Serial.println("   ]");
//
//
//  Serial.println();
// 
//
//      
//
//      
//                                                                                                                      // Define output variables from updated quaternion---these are Tait-Bryan angles, commonly used in aircraft orientation.
//                                                                                                                      // In this coordinate system, the positive z-axis is down toward Earth. 
//                                                                                                                      // Yaw is the angle between Sensor x-axis and Earth magnetic North 
//                                                                                                                      // (or true North if corrected for local declination, looking down on the sensor positive yaw is counterclockwise.
//                                                                                                                      // Pitch is angle between sensor x-axis and Earth ground plane, toward the Earth is positive, up toward the sky is negative.
//                                                                                                                      // Roll is angle between sensor y-axis and Earth ground plane, y-axis up is positive roll.
//                                                                                                                      // These arise from the definition of the homogeneous rotation matrix constructed from quaternions.
//                                                                                                                      // Tait-Bryan angles as well as Euler angles are non-commutative; that is, 
//                                                                                                                      // to get the correct orientation the rotations must be
//                                                                                                                      // applied in the correct order which for this configuration is yaw, pitch, and then roll.
//                                                                                                                      // For more see http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles which has additional links.
//  yaw   = atan2(2.0f * (q[1] * q[2] + q[0] * q[3]), q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]);   
//  pitch = -asin(2.0f * (q[1] * q[3] - q[0] * q[2]));
//  roll  = atan2(2.0f * (q[0] * q[1] + q[2] * q[3]), q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]);
//  pitch *= 180.0f / PI;
//  yaw   *= 180.0f / PI; 
//  roll  *= 180.0f / PI;
//    
//  Serial.println(""); 
//
//  Serial.print("Software Yaw, Pitch, Roll: ");  Serial.print(yaw, 2);   Serial.print(", ");   Serial.print(pitch, 2);   Serial.print(", ");   Serial.println(roll, 2);
//  
//  Serial.print("Hardware Yaw, Pitch, Roll: ");  Serial.print(Yaw, 2);   Serial.print(", ");   Serial.print(Pitch, 2);   Serial.print(", ");   Serial.println(Roll, 2);
//
//  Serial.println();
//
//  Serial.print("Hardware x, y, z linear acceleration: ");  Serial.print(LIAx, 2);  Serial.print(", ");   Serial.print(LIAy, 2);  Serial.print(", ");   Serial.println(LIAz, 2);
//
//  Serial.print("Hardware x, y, z gravity vector: ");  Serial.print(GRVx, 2);  Serial.print(", ");   Serial.print(GRVy, 2);  Serial.print(", ");   Serial.println(GRVz, 2);
//
//  Serial.println("");
//  Serial.print("Rate = ");  Serial.print((float)sumCount/sum, 2);   Serial.println(" Hz");
//
//  count = millis(); 
//  sumCount = 0;
//  sum = 0;    
//  
//





  
   
//                                //
//                                // BME280
//                                //      Temperature, humidity, and pressure sensor
//                                //
//                                //      Temperature is floating point, in Centigrade. 
//                                //      Pressure is a 32 bit integer with the pressure in Pascals.
//                                //      Humidity is in % Relative Humidity
//                                // 
//
//  Serial.println(""); Serial.println("");
//  Serial.println(" BME280 readings");
//  Serial.println("================");
//  Serial.println();                                
 
  Serial.print("BME280 temperature = "); Serial.print(bme280.readTemperature());             Serial.println(" [degrees C]   ");            // [Celcius]
  Serial.print("BME280 pressure = ");    Serial.print(bme280.readPressure()/ 100.0F);        Serial.println(" [hPa]   ");                  // [hPa]
  Serial.print("BME280 humidity = ");    Serial.print(bme280.readHumidity());                Serial.println(" [%]   ");                  // [%]
//     
  Serial.print("BME280 calculated altitude = "); Serial.print(bme280.readAltitude(SEALEVELPRESSURE_HPA));  Serial.println(" [m]      "); // [m] (approximate)
   
 //Serial.println(bme280.readTemperature()); 




                                //
                                // BH1730
                                //      Lux sensor
                                //
                                //      Asynchronous reading is possible. Value returned in Lux.
                                //


//  Serial.println(""); Serial.println("");
//  Serial.println(" BH1730 readings");
//  Serial.println("================");
//  Serial.println();                                                              
  
//  lux = BH1730.readLightLevel();                           
//  Serial.print("Light level = ");   Serial.print(lux);   Serial.println(" [Lux]");






   
                                //
                                // EKMC1603112
                                //      PIR hot body motion sensor
                                //

                                
//  Serial.println(); Serial.println();
//  Serial.println(" EKMC1603112 readings");
//  Serial.println("=====================");
//  Serial.println(); 
//   
//  if(EKMC1603112interruptCounter>0){
//    portENTER_CRITICAL(&mux);                                 // Disable interrupts when writing on a variable that is 
//                                                              //  shared with an interrupt to ensure that there is no concurrent access to it between the main code and the ISR.
//    EKMC1603112interruptCounter = 0;                          // Although we keep count of the number of PIR-caused interrupts, we have no need for that data at this time and therefore discard them.
//    portEXIT_CRITICAL(&mux);
//    Serial.println("warm body motion DETECTED");
//    digitalWrite(LED_PIN_ORANGE,  HIGH);  delay(10);  digitalWrite(LED_PIN_ORANGE,  LOW);     
//  } else {
//        Serial.println("no warm body motion detected");
//  }
  
}
