void update_actuators(){

  // 
  //  The code below should be refined for the applicable use cases
  //
  
  Serial.println("Updating Actuators");
  
  Serial.print("  Turning aircon ON...");           
  sendIR(poweron,228);                              // Turn ON the airconditioner
  Serial.println("      OK");


  //turn ON the leds
  digitalWrite(LED_PIN_RED,     HIGH);   // turn the LED on
  digitalWrite(LED_PIN_ORANGE,  HIGH);   // turn the LED on  
  digitalWrite(LED_PIN_GREEN,   HIGH);   // turn the LED on

  //Beep once
  digitalWrite(BUZZER, HIGH); delay(20); digitalWrite(BUZZER, LOW); 

  Serial.print("  Turning aircon OFF...");
  sendIR(poweroff,228);                             // Turn OFF the airconditioner
  Serial.println("      OK");

  //turn OFF the leds
  digitalWrite(LED_PIN_RED,     LOW);   // turn the LED off
  digitalWrite(LED_PIN_ORANGE,  LOW);   // turn the LED off  
  digitalWrite(LED_PIN_GREEN,   LOW);   // turn the LED off
  digitalWrite(IR_LED_FRONT,    LOW);   // turn the LED off  
  digitalWrite(IR_LED_TOP,      LOW);   // turn the LED off


  // give two beeps - a debugging aid
  digitalWrite(BUZZER, HIGH); delay(20); digitalWrite(BUZZER, LOW); 
  delay(200);
  digitalWrite(BUZZER, HIGH); delay(20); digitalWrite(BUZZER, LOW); 
      
}
